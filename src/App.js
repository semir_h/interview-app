import React from 'react';
import SidebarNavigation from './components/navigation/sidebarNavigation';
import Navbar from './components/navigation/navbar';
import Dashboard from './components/dashboard/dashboard';

import './App.scss';


function App() {
  return (
    <div className="App">
      <Navbar />
      <SidebarNavigation />
      <Dashboard />
    </div>
  );
}

export default App;
