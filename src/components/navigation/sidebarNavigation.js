import React, { Component } from "react";
import CollapseList from "./collapseList";
import { FontAwesomeIcon } from "../../system/fontawesome";
// Dummy data for sidebar navigation list
const sidebarNavigation = require("../../system/sidebarNavigation.json");

class SidebarNavigation extends Component {

  render() {
      let navigationListItems = sidebarNavigation.sidebarNavigation;
    return (
      <ul className="sidebar-navigation">
        {navigationListItems.map(item => {
          return (
            <li key={item.id}>
              {item.subItems ? (
                <CollapseList
                  linkText={item.name}
                  linkIcon={item.icon}
                  listItems={item.subItems}
                />
              ) : (
                <React.Fragment>
                  <span>
                    <FontAwesomeIcon icon={item.icon} />
                  </span>
                  {item.name}
                  <span>
                    <FontAwesomeIcon icon={"chevron-down"} />{" "}
                  </span>
                </React.Fragment>
              )}
            </li>
          );
        })}
      </ul>
    );
  }
}

export default SidebarNavigation;
