import React, { Component } from "react";
import { FontAwesomeIcon } from "../../system/fontawesome";
import logo from "../../images/logo.png";

class Navbar extends Component {

  // Format date for logged in user in navigation menu  
  formatDate() {
    let date = new Date();
    let dateArray = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "Mai",
      "Juni",
      "Juli",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    let d = date.getDate();
    let m = dateArray[date.getMonth()];
    let y = date.getFullYear();
    let h = date.getHours();
    let min = date.getMinutes();
    // return formated date
    return ("" + (d <= 9 ? "0" + d : d) + "." + m + " " + y + " " + (h <= 9 ? "0" + h : h) + ":" + (min <= 9 ? "0" + min : min));
  }
  render() {
    const currentDate = this.formatDate();
    return (
      <nav className="navbar">
        <div className="navbar-collapse">
          <button>
            <FontAwesomeIcon icon={"bars"} />
          </button>
          <a className="navbar-brand">
            <img src={logo} />
          </a>
        </div>

        <div className="navbar-title">
          <a className="nav-link" href="#">
            WebView
          </a>
        </div>
        <div className="navbar-right">
          <div className="navbar-info-icons">
            <FontAwesomeIcon
              className="inactive"
              icon={"exclamation-triangle"}
            />
            <FontAwesomeIcon className="inactive" icon={"times-circle"} />
            <FontAwesomeIcon className="inactive" icon={"info-circle"} />
            <FontAwesomeIcon icon={"question-circle"} />
          </div>
          <div className="navbar-login">
            <FontAwesomeIcon icon={"user"} />
            <div className="navbar-login-info">
              <span>John Doe</span>
              <span className="date">{currentDate}</span>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
