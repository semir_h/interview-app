import React, { Component } from "react";
import { FontAwesomeIcon } from "../../system/fontawesome";

// Collapse list for collapsing sidebar navigation menu
class CollapseList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: false,
      listItems: []
    };
  }

  // Expand list of items (collapse)
  activateCollapse(e) {
    if (e) e.preventDefault();
    this.setState({ isCollapsed: !this.state.isCollapsed });
    if (this.props.collapsedState)
      this.props.collapsedState(this.state.isCollapsed);
  }

  // Return collapsed list of items
  renderCollapsedListItems() {
    if (this.props.listItems && this.props.listItems.length) {
      const collapsedListItems = this.props.listItems.map((item, index) => {
        return (
          <li key={index}>
            <span>
              {" "}
              <FontAwesomeIcon icon={item.icon} />{" "}
            </span>
            {item.name}
          </li>
        );
      });

      return collapsedListItems;
    } else {
      return null;
    }
  }

  render() {
    let collapseClass =
      "sidebar-submenu-navigation" + (this.state.isCollapsed ? " show" : "");
    let collapseIcon = this.state.isCollapsed ? "chevron-up" : "chevron-down";
    return (
      <React.Fragment>
        <div onClick={e => this.activateCollapse(e)}>
          <span>
            {" "}
            <FontAwesomeIcon icon={this.props.linkIcon} />{" "}
          </span>
          {this.props.linkText}
          <span>
            {" "}
            <FontAwesomeIcon icon={collapseIcon} />{" "}
          </span>
        </div>
        <ul className={collapseClass}>
          {this.renderCollapsedListItems()}
          {this.props.children}
        </ul>
      </React.Fragment>
    );
  }
}

export default CollapseList;
