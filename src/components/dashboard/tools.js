import React, { Component } from "react";
import { FontAwesomeIcon } from "../../system/fontawesome";

// Tool list item data source
const toolItems = require("../../system/tools.json");
const toolItems1 = require("../../system/tools1.json");
const toolItems2 = require("../../system/tools2.json");
const toolItems3 = require("../../system/tools3.json");

class Tools extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      filteredItems: [],
      selectedTool: ""
    };
  }

  // Populate tool list of item with data before rendering
  componentWillMount() {
    this.handlePopulateToolsData();
    this.setState({
      filteredPoets: this.state.items
    });
  }

  // Filter Tool list items based on input search
  filterList = event => {
    let updatedList = this.state.items;
    updatedList = updatedList.filter(item => {
      return (
        item.name.toLowerCase().search(event.target.value.toLowerCase()) !== -1
      );
    });
    this.setState({ filteredItems: updatedList });
  };

  // Populate tool list of items based on which tool data source is passed from parent
  handlePopulateToolsData() {
      // Tool data source passed from parent
    const listData = this.props.toolListSrc;
    if (this.state.selectedTool !== listData) {
      let toolsList = [];
      switch (listData) {
        case "toolItems":
          toolsList = toolItems.tools;
          break;
        case "toolItems1":
          toolsList = toolItems1.tools;
          break;
        case "toolItems2":
          toolsList = toolItems2.tools;
          break;
        case "toolItems3":
          toolsList = toolItems3.tools;
          break;
        default:
          toolsList = toolItems.tools;
          break;
      }

      this.setState({
        items: toolsList,
        filteredItems: toolsList,
        selectedTool: listData
      });
    }
  }

  render() {
    this.handlePopulateToolsData();
    const items = this.state.filteredItems;
    const showToolsClass =
      "tools-filter-list " + (this.props.showTool ? "show" : "hide");
    return (
      <div className={showToolsClass}>
        <div className="tools-search">
          <label>Tools</label>
          <input
            type="text"
            placeholder="Filter tools"
            onChange={this.filterList}
          />
        </div>

        <ul className="tool-section">
          {items.map(item => {
            return (
              <li key={item.id}>
                {item.isAvailable ? (
                  <div className="tool-icon">
                    <FontAwesomeIcon icon={"check"} /> Available
                  </div>
                ) : (
                  <div className="tool-icon">
                    <FontAwesomeIcon
                      className={"error"}
                      icon={"times-circle"}
                    />{" "}
                    Not Available
                  </div>
                )}

                <div className="tool-content">
                  <h6>{item.name}</h6>
                  <span className="tool-class">
                    {" "}
                    Total Class: {item.class}{" "}
                  </span>
                  <span> {item.description} </span>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default Tools;
