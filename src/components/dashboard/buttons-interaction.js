import React, { Component } from "react";
import { FontAwesomeIcon } from "../../system/fontawesome";

class Dashboard extends Component {

  // When change button is clicked
  // Call parent function to cancel reordering of batches and return to default array
  handleCancelBatchReorder = () => {
    this.props.onHandleCancelBatchReorder();
  };
  // When Batch is selected, execute current Batch and update current Batch information
  handleExecuteTool = () => {
    this.props.onHandleRunTool();
  };

  // Called on mobile screen to return to Batch list
  handleGoBackButton = () => {
    this.props.onHideToolMobile();
  };

  render() {
    let disabledButton = this.props.disabledButton;
    let disabledClass = "updateBtn " + (disabledButton ? "disabled" : "");
    return (
      <div className="button-interaction">
        <button
          className={disabledClass}
          disabled={disabledButton}
          onClick={this.handleCancelBatchReorder}
        >
          <FontAwesomeIcon icon={"exchange-alt"} /> <span>Cancel</span>{" "}
        </button>
        <button
          className={disabledClass}
          disabled={disabledButton}
          onClick={this.handleCancelBatchReorder}
        >
          <FontAwesomeIcon icon={"play-circle"} />
          <span>Save</span>
        </button>
        <button>
          <FontAwesomeIcon icon={"trash-alt"} />
          <span>Abort</span>
        </button>
        <button>
          <FontAwesomeIcon icon={"paper-plane"} />
          <span>Create transport order</span>{" "}
        </button>
        <button>
          <FontAwesomeIcon icon={"print"} />
          <span>Print</span>
        </button>
        <button>
          <FontAwesomeIcon icon={"times"} />
          <span>Change</span>
        </button>
        <button className={"back-mobile"} onClick={this.handleGoBackButton}>
          <FontAwesomeIcon icon={"undo-alt"} />
          <span>Go Back</span>
        </button>
        <button onClick={this.handleExecuteTool}>
          <FontAwesomeIcon icon={"save"} />
          <span>Execute</span>
        </button>
      </div>
    );
  }
}

export default Dashboard;
