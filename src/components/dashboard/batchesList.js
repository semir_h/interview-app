import React, { Component } from "react";
import { FontAwesomeIcon } from "../../system/fontawesome";
import { SortableContainer, SortableElement } from "react-sortable-hoc";
import arrayMove from "array-move";
// Dummy data for batch list
const batches = require("../../system/batches.json");

// Populate Drag&Drop batch list data
const SortableItem = SortableElement(item => {
  // Get selected batch
  const onToolSelect = () => {
    item.onToolSelected(item.value);
  };
  // Check if current batch item is selected, and replace slot number 
  // with "check-square" icon
  const slotChecked =
    item.value.slot === 1 ? (
      item.value.slot
    ) : (
      <FontAwesomeIcon icon={"check-square"} />
    );
  // Check if current batch item is selected, 
  // if it is then add class 'selected-batch'.
  const selectedClass =
    "batch-item " + (item.value.slot !== 1 ? "selected-batch" : "");
  const lotName = item.value.lotName;
  const batchName = item.value.batchName;
  const quantity = item.value.quantity;
  return (
    // Bind onClick method to be executed when div is clicked
    <div className={selectedClass} onClick={onToolSelect}>
      <div>
        <FontAwesomeIcon icon={"expand-arrows-alt"} />
      </div>
      <div className="batch-slot">{slotChecked}</div>
      <div className="batch-lot">{lotName}</div>
      <div className="batch-name">{batchName}</div>
      {quantity}
      <div className="push-right">
        <FontAwesomeIcon icon={"chevron-right"} />
      </div>
    </div>
  );
});

// Container for Drag&Drop batch list data,
// items => Array(batch-object)
// onBatchItemSelected => ref passed from class render func
const SortableList = SortableContainer(({ items, onBatchItemSelected }) => {
  // Get selected batch
  const onToolSelected = item => {
    onBatchItemSelected(item);
  };
  return (
    // <Loop through batch list of items and append to 
    // sortable list
    <div className="batch-items">
      {items.map((value, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          value={value}
          onToolSelected={onToolSelected}
        />
      ))}
    </div>
  );
});

class BatchesList extends Component {
  // items => holds batch list of items
  // filteredItems => holds batch list of items, but on filtering list is being reduced
  state = {
    items: batches.batches,
    filteredItems: batches.batches
  };
  // When Drag&Drop is finished rearange array items
  onSortEnd = ({ oldIndex, newIndex }) => {
    this.setState(({ items }) => ({
      items: arrayMove(items, oldIndex, newIndex)
    }));
    // Call parent function
    this.props.onBatchisReordered();
  };

  // Check which Batch item is selected
  // Remove class from previously selected item
  // Add class to new selected item
  handleOnToolSelected = batchItem => {
    const checkBatchItem = "checked";
    const uncheckBatchItem = 1;
    let batchItems = this.state.items;
    // Loop through list of items and check for selected item
    batchItems.forEach((element, index) => {
      if (element.slot === checkBatchItem)
        batchItems[index].slot = uncheckBatchItem;
      if (element.id === batchItem.id) batchItems[index].slot = checkBatchItem;
    });
    // Update current array of Batch items
    this.setState({
      items: batchItems
    });
    // Calling parent function
    this.props.onBatchItemSelected(batchItem);
  };

  // Filter Batch list items based on input search
  filterList = event => {
    let updatedList = this.state.filteredItems;
    updatedList = updatedList.filter(item => {
      return (
        item.batchName
          .toLowerCase()
          .search(event.target.value.toLowerCase()) !== -1
      );
    });
    // Update current array of Batch items
    this.setState({ items: updatedList });
  };

  render() {
    // items => array of Batch list items
    let items = this.state.items;
    // Check if 
    console.log("Is batch diesabled => " + this.props.rtnToDefaultBatchOrder);
    if (this.props.rtnToDefaultBatchOrder) {
      this.setState({
        items: this.state.filteredItems
      });
      this.props.onCancelReorder();
    }
    const batchesClass =
      "batches-list " + (this.props.showTool ? "show" : "hide");
    return (
      <div className={batchesClass}>
        <div className="tools-search">
          <label>Following Batches</label>
          <input
            type="text"
            placeholder="Filter Batches"
            onChange={this.filterList}
          />
        </div>
        <SortableList
          distance={1}
          items={items}
          onSortEnd={this.onSortEnd}
          onBatchItemSelected={this.handleOnToolSelected}
        />
      </div>
    );
  }
}

export default BatchesList;
