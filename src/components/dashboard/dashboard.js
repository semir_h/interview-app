import React, { Component } from "react";
import BatchesList from "./batchesList";
import ToolList from "./tools";
import Planning from "./planningTimer";
import ButtonsInteraction from "./buttons-interaction";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toolListSrc: "",
      disabledButton: true,
      showTool: false,
      lastSelectedTool: [],
      rtnToDefaultBatchOrder: false,
      runTool: []
    };
  }

  // When Batch item is selected update tool list source and show tools from selected batch
  onBatchItemSelected = newTool => {
    let showTool = true;
    if (
      this.state.showTool &&
      newTool.tools === this.state.lastSelectedTool.tools
    )
      showTool = false;
    this.setState({
      toolListSrc: newTool.tools,
      showTool: showTool,
      lastSelectedTool: newTool
    });
  };

  // Enable "Cancel", "Save" Button when Batch is manually sorted
  onBatchisReordered = () => {
    this.setState({
      disabledButton: false
    });
  };
  // Cancel reordering of batches and return to default array and set button to disabled
  // Called from Buttons Component
  onHandleCancelBatchReorder = () => {
    this.setState({
      rtnToDefaultBatchOrder: true,
      disabledButton: true
    });
  };
  
  onHandleCancelReorderingOfBatches = () => {
    this.setState({
      rtnToDefaultBatchOrder: false
    });
  };

  // When Batch is selected, execute current Batch and update current Batch information
  // Called from Buttons component
  onHandleRunTool = () => {
    this.setState({
      runTool: this.state.lastSelectedTool
    });
  };

  // Called on mobile screen to return to Batch list
  // Called from Buttons component
  onHideToolMobile = () => {
    this.setState({
      showTool: false
    });
  };

  render() {
    let showToolClass =
      "dashboard bg-gray p-15 " + (this.state.showTool ? "showTool" : "");
    return (
      <div className={showToolClass}>
        <Planning runningTool={this.state.runTool} />

        <ButtonsInteraction
          disabledButton={this.state.disabledButton}
          onHandleCancelBatchReorder={this.onHandleCancelBatchReorder}
          onHandleRunTool={this.onHandleRunTool}
          onHideToolMobile={this.onHideToolMobile}
        />

        <div className="batch-container">
          <BatchesList
            onBatchItemSelected={this.onBatchItemSelected}
            onBatchisReordered={this.onBatchisReordered}
            showTool={this.state.showTool}
            rtnToDefaultBatchOrder={this.state.rtnToDefaultBatchOrder}
            onCancelReorder={this.onHandleCancelReorderingOfBatches}
          />

          <ToolList
            toolListSrc={this.state.toolListSrc}
            showTool={this.state.showTool}
          />
        </div>
      </div>
    );
  }
}

export default Dashboard;
