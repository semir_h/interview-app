import React, { Component } from "react";
import "react-step-progress-bar/styles.css";
import { ProgressBar } from "react-step-progress-bar";

class PlanningTimer extends Component {
  render() {
    let hour = this.props.runningTool.progressHour
      ? this.props.runningTool.progressHour
      : 2;
    let minute = this.props.runningTool.progressMinute
      ? this.props.runningTool.progressMinute
      : 25;
    let timeText = "Remaining time: " + hour + "h" + minute + "m";
    let amount = this.props.runningTool.amount
      ? this.props.runningTool.amount
      : "7.200";
    let pieces = this.props.runningTool.pieces
      ? this.props.runningTool.pieces
      : "33.600";
    let lotName = this.props.runningTool.lotName
      ? this.props.runningTool.lotName
      : "Lot 8";
    let batchName = this.props.runningTool.batchName
      ? this.props.runningTool.batchName
      : "Prod 0052";
    let progressBar = this.props.runningTool.progressBar
      ? this.props.runningTool.progressBar
      : 25;
    return (
      <div className="planning-container">
        <div className="planing-title">
          <span>Current Branch</span>
          <h3>
            {lotName} / {batchName}{" "}
          </h3>
        </div>
        <div className="planning-timer">
          <ProgressBar
            percent={progressBar}
            filledBackground="#1461a9"
            text={timeText}
          />
        </div>
        <div className="planing-details">
          <span>Amount / Pieces</span>
          <h3>
            {amount} / {pieces}
          </h3>
        </div>
      </div>
    );
  }
}

export default PlanningTimer;
